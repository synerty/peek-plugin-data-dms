import logging

from peek_plugin_base.agent.PluginAgentEntryHookABC import PluginAgentEntryHookABC
from peek_plugin_data_dms._private.ImportDmsDataTuples import importDataDmsTuples

logger = logging.getLogger(__name__)


class PluginAgentEntryHook(PluginAgentEntryHookABC):
    def load(self):
        importDataDmsTuples()
        logger.debug("Loaded")

    def start(self):
        logger.debug("Started")

    def stop(self):
        logger.debug("Stopped")

    def unload(self):
        logger.debug("Unloaded")
