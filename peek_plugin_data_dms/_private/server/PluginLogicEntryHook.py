import logging

from peek_plugin_base.server.PluginLogicEntryHookABC import PluginLogicEntryHookABC
from peek_plugin_base.server.PluginServerStorageEntryHookABC import (
    PluginServerStorageEntryHookABC,
)
from peek_plugin_base.server.PluginServerWorkerEntryHookABC import (
    PluginServerWorkerEntryHookABC,
)
from peek_plugin_data_dms._private.ImportDmsDataTuples import importDataDmsTuples

logger = logging.getLogger(__name__)


class PluginLogicEntryHook(
    PluginLogicEntryHookABC,
    PluginServerStorageEntryHookABC,
    PluginServerWorkerEntryHookABC,
):
    def load(self) -> None:
        importDataDmsTuples()
        logger.debug("loaded")

    def start(self):
        pass

    def stop(self):
        pass

    def unload(self):
        pass

    ###### Implement PluginServerStorageEntryHookABC

    @property
    def dbMetadata(self):
        from peek_plugin_data_dms._private.storage import DeclarativeBase

        return DeclarativeBase.metadata

    ###### Implement PluginServerWorkerEntryHookABC
