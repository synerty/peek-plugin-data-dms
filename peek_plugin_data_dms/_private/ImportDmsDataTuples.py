from txhttputil.util.ModuleUtil import filterModules


def importDataDmsTuples():
    from peek_plugin_data_dms.data import (
        job,
        call,
        incident,
        user,
        zone,
        equipment,
        customer,
        system_parameter,
    )
    from peek_plugin_data_dms.data.job import dispatch
    from peek_plugin_data_dms.data.call import dispatch
    from peek_plugin_data_dms.data.incident import dispatch, finding
    from peek_plugin_data_dms.data import assessment

    packages = (
        job,
        job.dispatch,
        call,
        call.dispatch,
        incident,
        incident.dispatch,
        incident.finding,
        user,
        zone,
        equipment,
        customer,
        assessment,
        system_parameter,
    )

    for package in packages:
        for mod in filterModules(package.__name__, package.__file__):
            __import__(mod, locals(), globals())
