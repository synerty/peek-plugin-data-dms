import logging

from peek_plugin_base.worker.PluginWorkerEntryHookABC import PluginWorkerEntryHookABC
from peek_plugin_data_dms._private.ImportDmsDataTuples import importDataDmsTuples

logger = logging.getLogger(__name__)


class PluginWorkerEntryHook(PluginWorkerEntryHookABC):
    def load(self):
        importDataDmsTuples()
        logger.debug("loaded")

    def start(self):
        pass

    def stop(self):
        pass

    def unload(self):
        pass

    @property
    def celeryAppIncludes(self):
        return []
