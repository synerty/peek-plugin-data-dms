import logging
from datetime import datetime

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsLoggedInUserTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsLoggedInUserTuple"

    #:  This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField()

    #:  The name of the user that is logged in
    userName: str = TupleField()

    #:  The title of the user that is logged in
    userTitle: str = TupleField()

    #:  The device name logged into
    loggedInDevice: str = TupleField()

    #:  The datetime that the user logged in
    loggedInDate: datetime = TupleField()
