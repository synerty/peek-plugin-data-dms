import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsLoggedInUserTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsLoggedInUserTuple";

    // The name of the user that is logged in
    userName: string;

    // The title of the user that is logged in
    userTitle: string;

    // The device name logged into
    loggedInDevice: string;

    // The datetime that the user logged in
    loggedInDate: Date;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsLoggedInUserTuple.tupleName);
    }
}
