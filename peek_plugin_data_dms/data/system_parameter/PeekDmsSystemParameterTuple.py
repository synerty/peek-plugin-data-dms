from vortex.Tuple import Tuple
from vortex.Tuple import TupleField
from vortex.Tuple import addTupleType

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsSystemParameterTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsSystemParameterTuple"

    KEY_MOBILE_MSG_ALARM_TIMEOUT = "MOBILE_MSG_ALARM_TIMEOUT"

    #:  This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField()

    key: str = TupleField()
    value: int = TupleField()
    unit: str = TupleField()
