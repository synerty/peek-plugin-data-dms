import logging

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsEquipmentListItemTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsEquipmentListItemTuple"

    #:  This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField()

    #:  ID
    id: str = TupleField()

    #:  Alias
    alias: str = TupleField()

    #:  Name
    name: str = TupleField()
