import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsEquipmentListItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsEquipmentListItemTuple";

    // The ID
    id: string;

    // The Alias
    alias: string;

    // The name
    name: string;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsEquipmentListItemTuple.tupleName);
    }
}
