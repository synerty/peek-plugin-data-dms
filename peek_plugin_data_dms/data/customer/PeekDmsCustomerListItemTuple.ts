import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsCustomerListItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsCustomerListItemTuple";

    // The Customer ID
    customerId: string;

    // The name
    name: string;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsCustomerListItemTuple.tupleName);
    }
}
