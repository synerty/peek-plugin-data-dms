import logging
from datetime import datetime

from vortex.Tuple import Tuple
from vortex.Tuple import TupleField
from vortex.Tuple import addTupleType

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.assessment.PeekDmsAssessmentPhotoDataTuple import (
    PeekDmsAssessmentPhotoBase64DataTuple,
)

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsAssessmentPhotoTuple(Tuple):
    """Photo metadata

    :param key: str: the key of photo
    :param photoNumber: int: the number tag in a damage item e.g. 1,2,3...
    :param dateTime: datetime: the time-zoned datetime uploaded
    :param photoMimeType: str: the mime type of the photo e.g. "image/png"
    :param relativePath: str: relative path to the assessment/incident that this photo belongs to
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsAssessmentPhotoTuple"

    key: str = TupleField(comment="the key of photo")
    photoNumber: int = TupleField(comment="the number tag in a damage item")
    dateTime: datetime = TupleField(comment="the datetime uploaded")
    photoMimeType: str = TupleField(comment="the mime type of the photo")
    relativePath: str = TupleField(
        comment="relative path to the assessment/incident that this photo belongs to"
    )

    def toPeekDmsAssessmentPhotoBase64DataTuple(self, imageBase64: str):
        tuple_ = PeekDmsAssessmentPhotoBase64DataTuple()
        tuple_.relativePath = self.relativePath
        tuple_.photoMimeType = self.photoMimeType
        tuple_.dateTime = self.dateTime
        tuple_.photoMimeType = self.photoMimeType
        tuple_.key = self.key
        tuple_.imageBase64 = imageBase64
        return tuple_
