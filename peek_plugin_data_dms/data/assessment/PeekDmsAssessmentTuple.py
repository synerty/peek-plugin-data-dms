import logging
from datetime import datetime
from typing import List

from vortex.Tuple import Tuple
from vortex.Tuple import TupleField
from vortex.Tuple import addTupleType

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.assessment.PeekDmsAssessmentDamagedItemTuple import (
    PeekDmsAssessmentDamagedItemTuple,
)

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsAssessmentParamsTuple(Tuple):
    """Assessment tuple with damaged item list

    :param assessmentId: str: the obfuscated identifier of an assessment e.g. "9L"
    :param incidentId: str: incidentId in string e.g. "a00000001INCD"
    :param createdDate: datetime: the time-zoned datetime the assessment was created
    :param updatedDate：datetime: the time-zoned datetime the assessment was last updated
    :param damagedItemList: List[PeekDmsAssessmentDamagedItemTuple]: a list of damaged items
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsAssessmentParamsTuple"

    incidentId: str = TupleField(comment="incident id")


@addTupleType
class PeekDmsAssessmentTuple(Tuple):
    """Assessment tuple with damaged item list

    :param assessmentId: str: the obfuscated identifier of an assessment e.g. "9L"
    :param incidentId: str: incidentId in string e.g. "a00000001INCD"
    :param createdDate: datetime: the time-zoned datetime the assessment was created
    :param updatedDate：datetime: the time-zoned datetime the assessment was last updated
    :param damagedItemList: List[PeekDmsAssessmentDamagedItemTuple]: a list of damaged items
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsAssessmentTuple"

    assessmentId: str = TupleField(comment="assessment id")
    incidentId: str = TupleField(comment="incident id")
    createdDate: datetime = TupleField(
        comment="the time-zoned datetime the assessment was created"
    )
    updatedDate: datetime = TupleField(
        comment="the time-zoned datetime the assessment was last updated"
    )
    damagedItemList: List[PeekDmsAssessmentDamagedItemTuple] = TupleField(
        comment="a list of damaged items", defaultValue=[]
    )
