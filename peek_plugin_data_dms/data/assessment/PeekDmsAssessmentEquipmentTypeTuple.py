import logging
from datetime import datetime

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsAssessmentEquipmentTypeTuple(Tuple):
    """ Enum of equipment types

    :param equipmentType: str: equipment type in string
    :param order: int: the display order of the type
    :param hasPhase: bool: true if the equipment has phase property; false otherwise
    :param hasVoltage: bool: true if if the equipment has voltage property; false otherwise
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsAssessmentEquipmentTypeTuple"

    equipmentType: str = TupleField(comment="equipment type in string")
    order: int = TupleField(comment="the display order of the type")
    hasPhase: bool = TupleField(comment="if the equipment has phase property")
    hasVoltage: bool = TupleField(comment="if the equipment has voltage property")
