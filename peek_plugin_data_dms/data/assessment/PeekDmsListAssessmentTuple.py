from typing import List

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsListAssessmentParamsTuple(Tuple):
    """A slice of assessments

    :param page: int: the index number of page, starting from 0
    :param pageSize: int: the size of a page
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsListAssessmentParamsTuple"

    page: int = TupleField(comment="the number of page, starting from 0")
    pageSize: int = TupleField(comment="the size of a page")


@addTupleType
class PeekDmsListAssessmentTuple(Tuple):
    """A slice of assessments

    :param page: int: the index number of page, starting from 0
    :param pageSize: int: the size of a page
    :param count: int: the number of assessments in this tuple
    :param assessments: List[dict]: a list of assessment objects
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsListAssessmentTuple"

    page: int = TupleField(comment="the number of page, starting from 0")
    pageSize: int = TupleField(comment="the size of a page")
    count: int = TupleField(comment="the number of assessments in this tuple")
    assessments: List[dict] = TupleField(comment="a list of assessment objects")
