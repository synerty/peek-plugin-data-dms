import logging
from datetime import datetime

from vortex.Tuple import addTupleType, TupleField, Tuple

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsAssessmentPhotoDataParamsTuple(Tuple):
    """Photo metadata with full URL path link

    :param photoNumber: int: the number tag in a damage item e.g. 1,2,3...
    :param dateTime: datetime: the time-zoned datetime uploaded
    :param photoMimeType: str: the mime type of the photo e.g. "image/png"
    :param relativePath: str: the relative path to the assessment/incident that this photo belongs to
    :param imageUrlPath: str: the url path of the photo withouthttp ip and port e.g. "/peek_plugin_enmac_field_assessments/photo/a00000001INCD/1.png"
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsAssessmentPhotoDataParamsTuple"

    id: str = TupleField(comment="obfuscated id")


@addTupleType
class PeekDmsAssessmentPhotoDataTuple(Tuple):
    """Photo metadata with full URL path link

    :param key: obfuscated key of a photo
    :param photoNumber: int: the number tag in a damage item e.g. 1,2,3...
    :param dateTime: datetime: the time-zoned datetime uploaded
    :param photoMimeType: str: the mime type of the photo e.g. "image/png"
    :param relativePath: str: the relative path to the assessment/incident that this photo belongs to
    :param imageUrlPath: str: the url path of the photo withouthttp ip and port e.g. "/peek_plugin_enmac_field_assessments/photo/a00000001INCD/1.png"
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsAssessmentPhotoDataTuple"

    key: str = TupleField(comment="obfuscated key of a photo")
    photoNumber: int = TupleField(comment="the number tag in a damage item")
    dateTime: datetime = TupleField(comment="the datetime uploaded")
    photoMimeType: str = TupleField(comment="Mime type of the photo")
    relativePath: str = TupleField(
        comment="relative path to the "
        "assessment/incident that this "
        "photo belongs to"
    )
    imageUrlPath: str = TupleField(
        comment="url path of the photo without http ip and port"
    )

    def toPeekDmsAssessmentPhotoBase64DataTuple(self):
        tuple_ = PeekDmsAssessmentPhotoBase64DataTuple()
        tuple_.key = self.key
        tuple_.photoNumber = self.photoNumber
        tuple_.dateTime = self.dateTime
        tuple_.photoMimeType = self.photoMimeType
        tuple_.relativePath = self.relativePath
        return tuple_


@addTupleType
class PeekDmsAssessmentPhotoBase64DataTuple(Tuple):
    """Photo metadata with full URL path link

    :param key: obfuscated key of a photo
    :param photoNumber: int: the number tag in a damage item e.g. 1,2,3...
    :param dateTime: datetime: the time-zoned datetime uploaded
    :param photoMimeType: str: the mime type of the photo e.g. "image/png"
    :param relativePath: str: the relative path to the assessment/incident that this photo belongs to
    :param imageUrlPath: str: the url path of the photo withouthttp ip and port e.g. "/peek_plugin_enmac_field_assessments/photo/a00000001INCD/1.png"
    """

    __tupleType__ = dataDmsTuplePrefix + "AssessmentPhotoBase64DataTuple"

    key: str = TupleField(comment="obfuscated key of a photo")
    photoNumber: int = TupleField(comment="the number tag in a damage item")
    dateTime: datetime = TupleField(comment="the datetime uploaded")
    photoMimeType: str = TupleField(comment="Mime type of the photo")
    relativePath: str = TupleField(
        comment="relative path to the "
        "assessment/incident that this "
        "photo belongs to"
    )
    imageBase64: str = TupleField()
