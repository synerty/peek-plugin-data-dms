import logging
from typing import List

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.Tuple import Tuple
from vortex.Tuple import TupleField
from vortex.Tuple import addTupleType
from .PeekDmsAssessmentPhotoTuple import PeekDmsAssessmentPhotoTuple

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsAssessmentDamagedItemTuple(Tuple):
    """Damage item tuple with attached photo

    :param equipmentType: str: equipment type e.g. "Switch - ABI"
    :param details: str: details
    :param numRequired: int: the number of items required
    :param phases: int: the number of phases
    :param size: str: the size of the damaged item
    :param subType: str: the subtype of the damaged item
    :param voltage: str: the voltage that the item is applied to
    :param incidentId: str: incident id e.g. "a00000001INCD"
    :param incidentRef: str: the incident reference in string e.g. "INCD-1-a"
    :param photoList: List[PeekDmsAssessmentPhotoTuple]: the list of photos of damaged items
    """

    __tupleType__ = dataDmsTuplePrefix + "PeekDmsAssessmentDamagedItemTuple"

    equipmentType: str = TupleField(comment="equipment type")
    details: str = TupleField(comment="details")
    numRequired: int = TupleField(comment="the number of items required")
    phases: int = TupleField(comment="the number of phases")
    size: str = TupleField(comment="the size of the damaged item")
    subType: str = TupleField(comment="the subtype of the damaged item")
    voltage: int = TupleField(comment="the voltage that the item is applied to")
    incidentId: str = TupleField(comment="incident id")
    incidentRef: str = TupleField(comment="incident reference")
    photoList: List[PeekDmsAssessmentPhotoTuple] = TupleField(
        comment="the list of photos of damaged items", defaultValue=[]
    )
