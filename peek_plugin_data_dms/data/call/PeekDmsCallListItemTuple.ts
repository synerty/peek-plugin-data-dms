import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsCallListItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsCallListItemTuple";

    // Second class data support.
    data: { [key: string]: any } = {};

    callId: string;
    callNumber: string;
    incidentId: string;
    category: string;
    createdDate: Date;

    constructor() {
        super(PeekDmsCallListItemTuple.tupleName);
    }
}
