import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsCallCallbackTypeTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsCallCallbackTypeTuple";

    // This field allows customer specific data, that peek doesn't need to work
    data: { [key: string]: any } = {};

    id: string;
    name: string;

    constructor() {
        super(PeekDmsCallCallbackTypeTuple.tupleName);
    }
}
