import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Incident Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsCallCtrlUpdateAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsCallCtrlUpdateAction";

    userId: string;
    incidentId: string;
    callId: string;
    updateType: string;

    constructor() {
        super(PeekDmsCallCtrlUpdateAction.tupleName);
    }
}
