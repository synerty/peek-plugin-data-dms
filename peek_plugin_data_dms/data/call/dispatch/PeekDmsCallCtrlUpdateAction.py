from vortex.Tuple import addTupleType, Tuple, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsCallCtrlUpdateAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsCallCtrlUpdateAction"

    userId = TupleField(typingType=str)
    incidentId = TupleField(typingType=str)
    callId = TupleField(typingType=str)
    updateType = TupleField(typingType=str)
