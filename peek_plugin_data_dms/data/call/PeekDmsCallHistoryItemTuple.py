import logging
from datetime import datetime

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.Tuple import addTupleType, Tuple, TupleField

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsCallHistoryItemTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsCallHistoryItemTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField()

    callId: str = TupleField()
    callNumber: str = TupleField()
    incidentId: str = TupleField()
    category: str = TupleField()
    createdDate: datetime = TupleField()
