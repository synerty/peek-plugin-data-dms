import logging
from datetime import datetime

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsCallTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsCallTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(dict())

    callId: str = TupleField()
    callNumber: str = TupleField()
    incidentId: str = TupleField()

    operatingZone: str = TupleField()
    category: str = TupleField()

    priority: int = TupleField()
    isDanger: bool = TupleField()

    createdDate: datetime = TupleField()
    lastUpdatedDate: datetime = TupleField()
    closedDate: datetime = TupleField()

    #: A key that can be positioned on a diagram
    locationDisplayKey: str = TupleField()
