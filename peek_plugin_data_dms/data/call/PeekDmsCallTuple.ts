import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsCallTuple extends Tuple {
    public static readonly tupleName = dataDmsTuplePrefix + "PeekDmsCallTuple";

    // Second class data support.
    data: { [key: string]: any } = {};

    callId: string;
    callNumber: string;
    incidentId: string;

    operatingZone: string;
    category: string;

    priority: number;
    isDanger: boolean;

    createdDate: Date;
    lastUpdatedDate: Date;
    closedDate: Date;

    // A key that can be positioned on a diagram
    locationDisplayKey: string;

    constructor() {
        super(PeekDmsCallTuple.tupleName);
    }
}
