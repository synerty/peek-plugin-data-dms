import logging

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsCallContactTypeTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsCallContactTypeTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(comment="Second class data")

    id: str = TupleField()
    name: str = TupleField()
