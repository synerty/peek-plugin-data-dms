import logging
from datetime import datetime

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.job.PeekDmsJobOperationStateEnum import (
    PeekDmsJobOperationStateEnum,
)
from peek_plugin_data_dms.data.job.PeekDmsJobPermitTuple import PeekDmsJobPermitTuple

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsJobOperationTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobOperationTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data = TupleField(comment="Second class data", typingType=dict)

    jobId: str = TupleField()
    operationId: str = TupleField()
    operationNumber: float = TupleField()
    action: str = TupleField()
    permit: PeekDmsJobPermitTuple = TupleField()
    currentState: PeekDmsJobOperationStateEnum = TupleField()
    currentDisplayState: str = TupleField()

    comment: str = TupleField()

    componentId: str = TupleField()
    componentAlias: str = TupleField()
    componentDescription = TupleField()

    substationName: str = TupleField()
    substationNumber: str = TupleField()

    controlEngineer: str = TupleField()
    fieldEngineer: str = TupleField()
    fieldEngineerUserId: str = TupleField()

    instructSystemDate: datetime = TupleField()
    instructManualDate: datetime = TupleField()

    confirmSystemDate: datetime = TupleField()
    confirmManualDate: datetime = TupleField()

    def __init__(self, **kwargs):
        Tuple.__init__(self, **kwargs)

        if "permit" not in kwargs:
            self.permit = PeekDmsJobPermitTuple()
