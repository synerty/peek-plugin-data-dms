from typing import Optional

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.Tuple import Tuple, TupleField, addTupleType


@addTupleType
class PeekDmsJobOperationStateEnum(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobOperationStateEnum"

    NOT_SET = "NEW"

    # Completed states
    DEFAULT = "Default"
    ABORTED = "Aborted"
    EXECUTED = "Executed"
    COMPLETED = "Completed"
    FAILED = "Failed"
    REMOVED = "Removed"
    FIELD_CONFIRMED = "Field Confirmed"
    TENTATIVE_FIELD_CONFIRMED = "Tentative Field Confirmed"
    CONFIRMED = "Confirmed"

    # Instructed
    INSTRUCTED = "Instructed"
    ISSUED = "Issued"
    TRANSFER_1 = "Transfer-1"
    TRANSFER_4 = "Transfer-4"

    # Future
    ALL_PRE_CHECKED = "All-Pre-Checked"
    CREATED = "Created"
    INTENDED = "Intended"
    PROPOSED = "Proposed"

    #: The string value of the field status
    value: str = TupleField()

    def __init__(self, value: Optional[str] = None):
        Tuple.__init__(self)
        self.value = value
