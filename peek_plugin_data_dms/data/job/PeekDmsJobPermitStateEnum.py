from typing import Optional

from vortex.Tuple import Tuple, TupleField, addTupleType

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsJobPermitStateEnum(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobPermitStateEnum"

    value = TupleField(typingType=str, comment="The string value of the field status")

    def __init__(self, value: Optional[str] = None):
        Tuple.__init__(self)
        self.value = value
