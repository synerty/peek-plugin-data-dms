import logging
from datetime import datetime
from typing import Dict

from peek_plugin_data_dms.data.job.dispatch.PeekDmsJobFieldStatusEnum import (
    PeekDmsJobFieldStatusEnum,
)
from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsJobListItemTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobListItemTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: Dict = TupleField()

    #: The job id
    jobId: str = TupleField()

    #: The job number
    jobNumber: str = TupleField()

    #: The job name
    jobName: str = TupleField()

    #: The date the job is scheduled to start
    scheduledDate: str = TupleField()

    #: The status of the job in the field device.
    fieldStatus: PeekDmsJobFieldStatusEnum = TupleField()
