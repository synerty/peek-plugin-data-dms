import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobFieldRejectAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobFieldRejectAction";

    userId: string;
    jobId: string;
    reason: string;

    constructor() {
        super(PeekDmsJobFieldRejectAction.tupleName);
    }
}
