from vortex.Tuple import addTupleType, Tuple, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsJobCtrlDispatchAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobCtrlDispatchAction"

    userId = TupleField(typingType=str)
    jobId = TupleField(typingType=str)
    jobName = TupleField(typingType=str)
