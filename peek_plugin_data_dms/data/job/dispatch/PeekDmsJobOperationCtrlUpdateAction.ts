import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Job Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsJobOperationCtrlUpdateAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobOperationCtrlUpdateAction";

    userId: string;
    jobId: string;
    operationId: string;
    updateType: string;

    constructor() {
        super(PeekDmsJobOperationCtrlUpdateAction.tupleName);
    }
}
