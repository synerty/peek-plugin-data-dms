import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobFieldStartAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobFieldStartAction";

    userId: string;
    jobId: string;

    constructor() {
        super(PeekDmsJobFieldStartAction.tupleName);
    }
}
