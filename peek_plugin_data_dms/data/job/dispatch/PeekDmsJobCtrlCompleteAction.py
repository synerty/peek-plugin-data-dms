from vortex.Tuple import addTupleType, Tuple, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsJobCtrlCompleteAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobCtrlCompleteAction"

    userId = TupleField(typingType=str)
    jobId = TupleField(typingType=str)
