from vortex.Tuple import addTupleType, Tuple, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsJobCtrlUpdateAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobCtrlUpdateAction"

    userId = TupleField(typingType=str)
    jobId = TupleField(typingType=str)
