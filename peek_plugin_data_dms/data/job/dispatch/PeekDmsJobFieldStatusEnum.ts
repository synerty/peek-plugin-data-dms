import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobFieldStatusEnum extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobFieldStatusEnum";

    static readonly NOT_SET = "null";
    static readonly NEW = "NEW";
    static readonly DISPATCH_QUEUED = "DISPATCH_QUEUED";
    static readonly DISPATCHING = "DISPATCHING";
    static readonly DISPATCHED = "DISPATCHED";
    static readonly ACCEPTED = "ACCEPTED";
    static readonly REJECTED = "REJECTED";
    static readonly ACTIVE = "ACTIVE";
    static readonly TAKING_BACK = "TAKING_BACK";
    static readonly TAKEN_BACK = "TAKEN_BACK";
    static readonly HANDED_BACK = "HANDED_BACK";
    static readonly COMPLETED = "COMPLETED";

    // UI States
    static readonly STATE_CHANGE_PENDING = "STATE_CHANGE_PENDING";

    constructor(public value: string = PeekDmsJobFieldStatusEnum.NOT_SET) {
        super(PeekDmsJobFieldStatusEnum.tupleName);
    }

    get niceName(): string {
        let lookup = {};
        lookup[PeekDmsJobFieldStatusEnum.NEW] = "New";
        lookup[PeekDmsJobFieldStatusEnum.DISPATCH_QUEUED] = "Dispatch Queued";
        lookup[PeekDmsJobFieldStatusEnum.DISPATCHING] = "Dispatching";
        lookup[PeekDmsJobFieldStatusEnum.DISPATCHED] = "Dispatched";
        lookup[PeekDmsJobFieldStatusEnum.ACCEPTED] = "Accepted";
        lookup[PeekDmsJobFieldStatusEnum.REJECTED] = "Rejected";
        lookup[PeekDmsJobFieldStatusEnum.ACTIVE] = "Active";
        lookup[PeekDmsJobFieldStatusEnum.TAKING_BACK] = "Taking Back";
        lookup[PeekDmsJobFieldStatusEnum.TAKEN_BACK] = "Taken Back";
        lookup[PeekDmsJobFieldStatusEnum.HANDED_BACK] = "Handed Back";
        lookup[PeekDmsJobFieldStatusEnum.COMPLETED] = "Completed";

        // UI States
        lookup[PeekDmsJobFieldStatusEnum.STATE_CHANGE_PENDING] =
            "Change Pending";
        return lookup[this.value];
    }

    get isNew(): boolean {
        return (
            this.value === PeekDmsJobFieldStatusEnum.NEW ||
            this.value === PeekDmsJobFieldStatusEnum.DISPATCH_QUEUED ||
            this.value === PeekDmsJobFieldStatusEnum.DISPATCHING
        );
    }

    get isActive(): boolean {
        return this.value === PeekDmsJobFieldStatusEnum.ACTIVE;
    }

    get isAccepted(): boolean {
        return this.value === PeekDmsJobFieldStatusEnum.ACCEPTED;
    }

    get isRejected(): boolean {
        return this.value === PeekDmsJobFieldStatusEnum.REJECTED;
    }

    get isHandedBack(): boolean {
        return this.value === PeekDmsJobFieldStatusEnum.HANDED_BACK;
    }

    get isTakenBack(): boolean {
        return (
            this.value === PeekDmsJobFieldStatusEnum.TAKING_BACK ||
            this.value === PeekDmsJobFieldStatusEnum.TAKEN_BACK
        );
    }

    get isDispatched(): boolean {
        return (
            this.value === PeekDmsJobFieldStatusEnum.DISPATCHED ||
            this.value === PeekDmsJobFieldStatusEnum.DISPATCHING
        );
    }

    /** Is Filtered New
     *
     * This is used in the jobs filter.
     *
     * @returns {boolean}
     */
    get isFilteredNew(): boolean {
        return this.value === PeekDmsJobFieldStatusEnum.NEW;
    }

    /** Is Filtered Dispatched
     *
     * This is used in the job filter.
     *
     * @returns {boolean}
     */
    get isFilteredDispatched(): boolean {
        return (
            this.value === PeekDmsJobFieldStatusEnum.STATE_CHANGE_PENDING ||
            this.value === PeekDmsJobFieldStatusEnum.DISPATCH_QUEUED ||
            this.value === PeekDmsJobFieldStatusEnum.DISPATCHED ||
            this.value === PeekDmsJobFieldStatusEnum.DISPATCHING ||
            this.value === PeekDmsJobFieldStatusEnum.ACCEPTED ||
            this.value === PeekDmsJobFieldStatusEnum.ACTIVE
        );
    }
}
