import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobOperationFieldConfirmAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobOperationFieldConfirmAction";

    // Tuple Actions already have a dateTime field

    userId: string;
    jobId: string;
    jobNumber: string;
    operationId: string;
    requestFurtherInstructions: boolean;

    constructor() {
        super(PeekDmsJobOperationFieldConfirmAction.tupleName);
    }
}
