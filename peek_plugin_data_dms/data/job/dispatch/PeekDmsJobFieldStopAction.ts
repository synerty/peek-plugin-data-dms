import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobFieldStopAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobFieldStopAction";

    userId: string;
    jobId: string;
    reason: string;

    constructor() {
        super(PeekDmsJobFieldStopAction.tupleName);
    }
}
