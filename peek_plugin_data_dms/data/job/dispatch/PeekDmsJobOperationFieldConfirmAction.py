from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.TupleAction import TupleActionABC


@addTupleType
class PeekDmsJobOperationFieldConfirmAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobOperationFieldConfirmAction"

    userId = TupleField(typingType=str)
    jobId = TupleField(typingType=str)
    jobNumber = TupleField(typingType=str)
    operationId = TupleField(typingType=str)
    requestFurtherInstructions = TupleField(typingType=bool)
