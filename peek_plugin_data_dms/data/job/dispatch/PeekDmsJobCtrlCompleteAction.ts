import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Job Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsJobCtrlCompleteAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobCtrlCompleteAction";

    userId: string;
    jobId: string;

    constructor() {
        super(PeekDmsJobCtrlCompleteAction.tupleName);
    }
}
