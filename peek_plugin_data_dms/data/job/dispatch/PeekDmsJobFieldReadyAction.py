from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.TupleAction import TupleActionABC


@addTupleType
class PeekDmsJobFieldReadyAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobFieldReadyAction"

    userId: str = TupleField()
    jobId: str = TupleField()
    jobNumber: str = TupleField()

    #: Is the the second or subsequent time the controller has sent this
    stillWaiting: bool = TupleField(False)
