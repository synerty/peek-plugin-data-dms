from typing import Optional

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.Tuple import Tuple, TupleField, addTupleType


@addTupleType
class PeekDmsJobFieldStatusEnum(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobFieldStatusEnum"
    NEW = "NEW"
    DISPATCH_QUEUED = "DISPATCH_QUEUED"
    DISPATCHING = "DISPATCHING"
    DISPATCHED = "DISPATCHED"
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"
    ACTIVE = "ACTIVE"
    TAKING_BACK = "TAKING_BACK"
    TAKEN_BACK = "TAKEN_BACK"
    HANDED_BACK = "HANDED_BACK"
    COMPLETED = "COMPLETED"

    value = TupleField(typingType=str, comment="The string value of the field status")

    def __init__(self, value: Optional[str] = None):
        Tuple.__init__(self)
        self.value = value
