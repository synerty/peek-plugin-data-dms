import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Job - Field Hand Back
 *
 */
@addTupleType
export class PeekDmsJobFieldHandBackAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobFieldHandBackAction";

    userId: string;
    jobId: string;
    reason: string;

    constructor() {
        super(PeekDmsJobFieldHandBackAction.tupleName);
    }
}
