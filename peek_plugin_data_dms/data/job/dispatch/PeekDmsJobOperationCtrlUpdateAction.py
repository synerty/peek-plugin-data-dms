from vortex.Tuple import addTupleType, Tuple, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsJobOperationCtrlUpdateAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobOperationCtrlUpdateAction"

    userId = TupleField(typingType=str)
    jobId = TupleField(typingType=str)
    operationId = TupleField(typingType=str)
    updateType = TupleField(typingType=str)
