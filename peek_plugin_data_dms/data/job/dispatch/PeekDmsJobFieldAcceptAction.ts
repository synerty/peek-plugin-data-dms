import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Job Dispatch - Field Acknowledgment Action
 *
 */
@addTupleType
export class PeekDmsJobFieldAcceptAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobFieldAcceptAction";

    userId: string;
    jobId: string;

    constructor() {
        super(PeekDmsJobFieldAcceptAction.tupleName);
    }
}
