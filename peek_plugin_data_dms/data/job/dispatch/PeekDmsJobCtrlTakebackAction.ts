import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobCtrlTakebackAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobCtrlTakebackAction";

    userId: string;
    jobId: string;

    constructor() {
        super(PeekDmsJobCtrlTakebackAction.tupleName);
    }
}
