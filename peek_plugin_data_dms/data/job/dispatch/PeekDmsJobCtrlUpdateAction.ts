import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Job Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsJobCtrlUpdateAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobCtrlUpdateAction";

    userId: string;
    jobId: string;

    constructor() {
        super(PeekDmsJobCtrlUpdateAction.tupleName);
    }
}
