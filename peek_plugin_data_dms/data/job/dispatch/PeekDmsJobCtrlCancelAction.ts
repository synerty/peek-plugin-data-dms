import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Job Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsJobCtrlCancelAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobCtrlCancelAction";

    userId: string;
    jobId: string;

    constructor() {
        super(PeekDmsJobCtrlCancelAction.tupleName);
    }
}
