import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobFieldReadyAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobFieldReadyAction";

    userId: string;
    jobId: string;
    jobNumber: string;

    // Is the the second or subsequent time the controller has sent this
    stillWaiting: boolean = false;

    constructor() {
        super(PeekDmsJobFieldReadyAction.tupleName);
    }
}
