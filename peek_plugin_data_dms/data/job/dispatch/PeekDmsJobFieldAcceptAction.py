from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.TupleAction import TupleActionABC


@addTupleType
class PeekDmsJobFieldAcceptAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobFieldAcceptAction"

    userId = TupleField(typingType=str)
    jobId = TupleField(typingType=str)
