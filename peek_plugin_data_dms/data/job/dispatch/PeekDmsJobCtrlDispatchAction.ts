import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Job Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsJobCtrlDispatchAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobCtrlDispatchAction";

    userId: string;
    jobId: string;
    jobName: string;

    constructor() {
        super(PeekDmsJobCtrlDispatchAction.tupleName);
    }
}
