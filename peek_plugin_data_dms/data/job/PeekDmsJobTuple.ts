import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsJobFieldStatusEnum } from "./dispatch/PeekDmsJobFieldStatusEnum";

@addTupleType
export class PeekDmsJobTuple extends Tuple {
    public static readonly tupleName = dataDmsTuplePrefix + "PeekDmsJobTuple";

    jobId: string;
    jobNumber: string;
    jobType: string;
    jobName: string;
    workSummary: string;
    workDetail: string;
    scheduledStartDate: Date;
    scheduledEndDate: Date;

    fieldStatus: PeekDmsJobFieldStatusEnum = new PeekDmsJobFieldStatusEnum();

    omsIncidentNumber: string;
    activeControlEngineer: string;

    createdBy: string;
    createdDate: Date;

    approvedBy: string;
    approvedDate: Date;

    completedBy: string;
    completedDate: Date;

    fieldOperatives: any[];

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsJobTuple.tupleName);
    }
}
