import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobOperationStateEnum extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobOperationStateEnum";

    // NOTE, Not all the states are here because this is configurable in the DMS

    static readonly NOT_SET = "null";

    // Completed states
    static readonly ABORTED = "Aborted";
    static readonly EXECUTED = "Executed";
    static readonly COMPLETED = "Completed";
    static readonly FAILED = "Failed";
    static readonly REMOVED = "Removed";
    static readonly FIELD_CONFIRMED = "Field Confirmed";
    static readonly TENTATIVE_FIELD_CONFIRMED = "Tentative Field Confirmed";
    static readonly CONFIRMED = "Confirmed";

    // Instructed
    static readonly INSTRUCTED = "Instructed";
    static readonly ISSUED = "Issued";
    static readonly TRANSFER_1 = "Transfer-1";
    static readonly TRANSFER_4 = "Transfer-4";

    // Future
    static readonly DEFAULT = "Default";
    static readonly ALL_PRE_CHECKED = "All-Pre-Checked";
    static readonly CREATED = "Created";
    static readonly INTENDED = "Intended";
    static readonly PROPOSED = "Proposed";

    // UI States
    static readonly STATE_CHANGE_PENDING = "Change Pending";

    constructor(public value: string = PeekDmsJobOperationStateEnum.NOT_SET) {
        super(PeekDmsJobOperationStateEnum.tupleName);
    }

    get niceName(): string {
        return this.value;
    }
}
