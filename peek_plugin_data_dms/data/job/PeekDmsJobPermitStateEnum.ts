import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsJobPermitStateEnum extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobPermitStateEnum";

    // NOTE, Not all the states are here because this is configurable in the DMS

    static readonly NOT_SET = "null";

    // UI States

    constructor(public value: string = PeekDmsJobPermitStateEnum.NOT_SET) {
        super(PeekDmsJobPermitStateEnum.tupleName);
    }

    get niceName(): string {
        return this.value;
    }
}
