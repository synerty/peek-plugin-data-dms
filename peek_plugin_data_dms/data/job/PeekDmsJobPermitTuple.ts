import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsJobPermitStateEnum } from "./PeekDmsJobPermitStateEnum";

@addTupleType
export class PeekDmsJobPermitTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobPermitTuple";

    // Permit Details
    permitId: string;
    permitNumber: string;
    permitName: string;
    permitState: PeekDmsJobPermitStateEnum = new PeekDmsJobPermitStateEnum();
    permitLocation: string;

    // Equipment info
    equipmentId: string;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsJobPermitTuple.tupleName);
    }
}
