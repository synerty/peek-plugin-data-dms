import logging
from typing import Dict, Any

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.job.PeekDmsJobPermitStateEnum import (
    PeekDmsJobPermitStateEnum,
)
from vortex.Tuple import addTupleType, Tuple, TupleField

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsJobPermitTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobPermitTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: Dict[str, Any] = TupleField(comment="Second class data")

    # Permit Details
    permitId = TupleField(typingType=str)
    permitNumber = TupleField(typingType=str)
    permitName = TupleField(typingType=str)
    permitState = TupleField(typingType=PeekDmsJobPermitStateEnum)
    permitLocation = TupleField(typingType=str)

    # Equipment info
    equipmentId = TupleField(typingType=str)
