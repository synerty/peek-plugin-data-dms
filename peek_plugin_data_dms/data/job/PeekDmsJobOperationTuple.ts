import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsJobOperationStateEnum } from "./PeekDmsJobOperationStateEnum";
import { PeekDmsJobPermitTuple } from "./PeekDmsJobPermitTuple";

@addTupleType
export class PeekDmsJobOperationTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobOperationTuple";

    jobId: string;
    operationId: string;
    operationNumber: number;
    action: string;
    permit: PeekDmsJobPermitTuple = new PeekDmsJobPermitTuple();
    currentState: PeekDmsJobOperationStateEnum =
        new PeekDmsJobOperationStateEnum();
    currentDisplayState: string;

    comment: string;

    componentId: string;
    componentAlias: string;
    componentDescription: string;
    substationName: string;
    substationNumber: string;

    controlEngineer: string;
    fieldEngineer: string;
    fieldEngineerUserId: string;

    instructSystemDate: Date;
    instructManualDate: Date;

    confirmSystemDate: Date;
    confirmManualDate: Date;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsJobOperationTuple.tupleName);
    }

    get isPermit(): boolean {
        return !!this.permit?.permitId;
    }

    get isFuture(): boolean {
        return !this.isInstructed && !this.isFieldConfirmed && !this.isAborted;
    }

    get isInstructed(): boolean {
        // NOTE, Use PeekDmsOperationTuple.isInstructable(userId) instead
        return (
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.INSTRUCTED ||
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.TRANSFER_1 ||
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.TRANSFER_4 ||
            (this.currentState.value === PeekDmsJobOperationStateEnum.ISSUED &&
                this.action !== "Annotation")
        );
    }

    get isFieldConfirmed(): boolean {
        return (
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.STATE_CHANGE_PENDING ||
            this.currentState.value === PeekDmsJobOperationStateEnum.EXECUTED ||
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.COMPLETED ||
            this.currentState.value === PeekDmsJobOperationStateEnum.FAILED ||
            this.currentState.value === PeekDmsJobOperationStateEnum.REMOVED ||
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.FIELD_CONFIRMED ||
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.TENTATIVE_FIELD_CONFIRMED ||
            this.currentState.value ===
                PeekDmsJobOperationStateEnum.CONFIRMED ||
            (this.currentState.value === PeekDmsJobOperationStateEnum.ISSUED &&
                this.action === "Annotation")
        );
    }

    get isAborted(): boolean {
        return this.currentState.value === PeekDmsJobOperationStateEnum.ABORTED;
    }
}
