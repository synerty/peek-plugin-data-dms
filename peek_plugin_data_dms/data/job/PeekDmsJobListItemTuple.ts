import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsJobFieldStatusEnum } from "./dispatch/PeekDmsJobFieldStatusEnum";

@addTupleType
export class PeekDmsJobListItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsJobListItemTuple";

    jobId: string;
    jobName: string;
    jobNumber: string;
    scheduledDate: Date;
    fieldStatus: PeekDmsJobFieldStatusEnum = new PeekDmsJobFieldStatusEnum();

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsJobListItemTuple.tupleName);
    }
}
