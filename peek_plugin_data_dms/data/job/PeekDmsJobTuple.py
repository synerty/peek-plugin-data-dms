import logging
from datetime import datetime

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.job.dispatch.PeekDmsJobFieldStatusEnum import (
    PeekDmsJobFieldStatusEnum,
)

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsJobTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsJobTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data = TupleField(comment="Second class data", typingType=dict)

    jobId = TupleField(comment="The job id", typingType=str)

    #: The job type
    jobType: str = TupleField()

    #: The job name
    jobName: str = TupleField()

    jobNumber = TupleField(comment="The job number", typingType=str)
    workSummary = TupleField(typingType=str)
    workDetail = TupleField(typingType=str)
    scheduledStartDate = TupleField(
        comment="The date the job is scheduled to start", typingType=datetime
    )
    scheduledEndDate = TupleField(
        comment="The date the job is scheduled to end", typingType=datetime
    )

    fieldStatus = TupleField(
        typingType=PeekDmsJobFieldStatusEnum,
        comment="The status of the job in the field device.",
    )

    omsIncidentNumber = TupleField(typingType=str)
    activeControlEngineer = TupleField(typingType=str)

    createdBy = TupleField(typingType=str)
    createdDate = TupleField(typingType=datetime)

    approvedBy = TupleField(typingType=str)
    approvedDate = TupleField(typingType=datetime)

    completedBy = TupleField(typingType=str)
    completedDate = TupleField(typingType=datetime)
