import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsZoneListItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsZoneListItemTuple";

    // The name of the user that is logged in
    name: string;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsZoneListItemTuple.tupleName);
    }
}
