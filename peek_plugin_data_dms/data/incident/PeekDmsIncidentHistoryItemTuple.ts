import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentHistoryItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentHistoryItemTuple";

    incidentId: string;
    incidentNumber: string;
    incidentName: string;
    incidentCategory: string;

    createdDate: Date;
    scheduledDate: Date;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsIncidentHistoryItemTuple.tupleName);
    }
}
