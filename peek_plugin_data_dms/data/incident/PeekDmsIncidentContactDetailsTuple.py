from vortex.Tuple import addTupleType, TupleField, Tuple

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentContactDetailsTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentContactDetailsTuple"

    incidentId: str = TupleField()

    name: str = TupleField()
    address: str = TupleField()
    contactNumber: str = TupleField()
    vechicalDetails: str = TupleField()
    customerAgreementNumber: str = TupleField()
    statementAtSiteNumber: str = TupleField()
    emergencyServicesEventNumber: str = TupleField()
    comment: str = TupleField()

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(comment="Second class data")
