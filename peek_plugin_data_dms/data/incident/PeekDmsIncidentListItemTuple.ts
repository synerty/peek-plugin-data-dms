import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsIncidentFieldStatusEnum } from "./PeekDmsIncidentFieldStatusEnum";

@addTupleType
export class PeekDmsIncidentListItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentListItemTuple";

    incidentId: string;
    incidentNumber: string;
    incidentName: string;
    incidentCategory: string;

    createdDate: Date;
    scheduledDate: Date;

    fieldStatus: PeekDmsIncidentFieldStatusEnum =
        new PeekDmsIncidentFieldStatusEnum();

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsIncidentListItemTuple.tupleName);
    }
}
