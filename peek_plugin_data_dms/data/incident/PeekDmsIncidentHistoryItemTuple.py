import logging
from datetime import datetime

from .PeekDmsIncidentFieldStatusEnum import PeekDmsIncidentFieldStatusEnum
from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsIncidentHistoryItemTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentHistoryItemTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(comment="Second class data")

    incidentId: str = TupleField(comment="The incident id")
    incidentNumber: str = TupleField(comment="The incident name")
    incidentName: str = TupleField()
    incidentCategory: str = TupleField()

    createdDate: datetime = TupleField()
    scheduledDate: datetime = TupleField()
