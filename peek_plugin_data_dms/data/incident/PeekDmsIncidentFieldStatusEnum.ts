import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentFieldStatusEnum extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFieldStatusEnum";

    static readonly NOT_SET = "null";
    static readonly ASSIGNED = "ASSIGNED";
    static readonly ACCEPTED = "ACCEPTED";
    static readonly COMPLETED = "COMPLETED";
    static readonly DISPATCHED = "DISPATCHED";
    static readonly EN_ROUTE = "EN_ROUTE";
    static readonly ON_SITE = "ON_SITE";
    static readonly CANCELLED = "CANCELLED";
    static readonly STOPPED = "STOPPED";
    static readonly UNASSIGNED = "UNASSIGNED";
    static readonly REQUESTED = "REQUESTED";

    // UI States
    static readonly STATE_CHANGE_PENDING = "STATE_CHANGE_PENDING";

    constructor(public value: string = PeekDmsIncidentFieldStatusEnum.NOT_SET) {
        super(PeekDmsIncidentFieldStatusEnum.tupleName);
    }

    get niceName(): string {
        let lookup = {};
        lookup[PeekDmsIncidentFieldStatusEnum.NOT_SET] = "Null";
        lookup[PeekDmsIncidentFieldStatusEnum.ASSIGNED] = "Assigned";
        lookup[PeekDmsIncidentFieldStatusEnum.ACCEPTED] = "Accepted";
        lookup[PeekDmsIncidentFieldStatusEnum.COMPLETED] = "Completed";
        lookup[PeekDmsIncidentFieldStatusEnum.DISPATCHED] = "On My Way";
        lookup[PeekDmsIncidentFieldStatusEnum.EN_ROUTE] = "On My Way";
        lookup[PeekDmsIncidentFieldStatusEnum.ON_SITE] = "On Site";
        lookup[PeekDmsIncidentFieldStatusEnum.CANCELLED] = "Cancelled";
        lookup[PeekDmsIncidentFieldStatusEnum.STOPPED] = "Stopped";
        lookup[PeekDmsIncidentFieldStatusEnum.UNASSIGNED] = "Unassigned";

        // Unused ?
        lookup[PeekDmsIncidentFieldStatusEnum.REQUESTED] = "Requested";

        // UI States
        lookup[PeekDmsIncidentFieldStatusEnum.STATE_CHANGE_PENDING] =
            "Change Pending";
        return lookup[this.value];
    }

    get isUnassigned(): boolean {
        return (
            this.value === PeekDmsIncidentFieldStatusEnum.UNASSIGNED ||
            this.value === PeekDmsIncidentFieldStatusEnum.NOT_SET
        );
    }

    get isAssigned(): boolean {
        return this.value === PeekDmsIncidentFieldStatusEnum.ASSIGNED;
    }

    get isActive(): boolean {
        return (
            this.value === PeekDmsIncidentFieldStatusEnum.DISPATCHED ||
            this.value === PeekDmsIncidentFieldStatusEnum.EN_ROUTE ||
            this.value === PeekDmsIncidentFieldStatusEnum.ON_SITE
        );
    }

    get isAccepted(): boolean {
        return this.value === PeekDmsIncidentFieldStatusEnum.ACCEPTED;
    }

    get isRejected(): boolean {
        return this.value === PeekDmsIncidentFieldStatusEnum.UNASSIGNED;
    }

    get isDispatched(): boolean {
        return (
            this.value === PeekDmsIncidentFieldStatusEnum.DISPATCHED ||
            this.value === PeekDmsIncidentFieldStatusEnum.EN_ROUTE
        );
    }

    get isOnSite(): boolean {
        return this.value === PeekDmsIncidentFieldStatusEnum.ON_SITE;
    }

    get isStopped(): boolean {
        return this.value === PeekDmsIncidentFieldStatusEnum.STOPPED;
    }
}
