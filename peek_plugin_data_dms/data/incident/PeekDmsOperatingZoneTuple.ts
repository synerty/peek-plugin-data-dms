import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsOperatingZoneTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsOperatingZoneTuple";

    // This field allows customer specific data, that peek doesn't need to work
    data: { [key: string]: any } = {};

    id: string;
    name: string;

    constructor() {
        super(PeekDmsOperatingZoneTuple.tupleName);
    }
}
