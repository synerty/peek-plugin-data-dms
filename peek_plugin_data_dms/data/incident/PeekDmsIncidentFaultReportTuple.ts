import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentFaultReportTuple extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFaultReportTuple";

    incidentId: string;
    reportId: string;

    workType: string;
    workTypeTitle: string;

    failedAsset: string;
    failedAssetTitle: string;

    failureMode: string;
    failureModeTitle: string;

    failureComment: string;

    causeGroup: string;
    causeGroupTitle: string;

    cause: string;
    causeTitle: string;

    causeComment: string;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsIncidentFaultReportTuple.tupleName);
    }

    get isFullyPopulated(): boolean {
        return (
            this.workType != null &&
            this.workType.length != 0 &&
            this.failedAsset != null &&
            this.failedAsset.length != 0 &&
            this.failureMode != null &&
            this.failureMode.length != 0 &&
            this.cause != null &&
            this.cause.length != 0 &&
            this.causeGroup != null &&
            this.causeGroup.length != 0
        );
    }
}
