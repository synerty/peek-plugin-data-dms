import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsIncidentFieldStatusEnum } from "./PeekDmsIncidentFieldStatusEnum";

@addTupleType
export class PeekDmsIncidentTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentTuple";

    // This field allows customer specific data, that peek doesn't need to work
    data: { [key: string]: any } = {};

    incidentId: string;
    incidentNumber: string;
    incidentCategory: string;
    incidentType: string;
    priority: number;
    status: string;
    isSensitive: boolean;

    // The name of the dispatcher sending the job
    dispatcherName: string;

    // The name of the field resource the work is assigned to
    fieldAssignee: string;

    createdDate: Date;
    receivedDate: Date;
    estimatedRestorationDate: Date;
    restorationDate: Date;
    scheduledDate: Date;
    startDate: Date;
    completedDate: Date;
    lastUpdatedDate: Date;

    isPlanned: boolean;
    faultNumber: string;
    operatingZone: string;
    isConfirmed: boolean;

    // The status of the incident in the field device.
    fieldStatus: PeekDmsIncidentFieldStatusEnum =
        new PeekDmsIncidentFieldStatusEnum();

    incidentName: string;
    message1: string;
    message2: string;
    comment: string;

    primaryFeederName: string;
    primaryName: string;
    primaryAlias: string;
    primaryId: string;
    secondaryName: string;
    secondaryAlias: string;
    secondaryId: string;
    componentDescription: string;

    deadDeviceAlias: string;
    deadDeviceId: string;
    isDeviceDeadConfirmed: boolean;

    constructor() {
        super(PeekDmsIncidentTuple.tupleName);
    }
}
