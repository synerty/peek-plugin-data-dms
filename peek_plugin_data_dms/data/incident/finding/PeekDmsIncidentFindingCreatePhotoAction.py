from vortex.Tuple import addTupleType, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentFindingCreatePhotoAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFindingCreatePhotoAction"

    userId: str = TupleField()
    userDisplayName: str = TupleField()
    incidentId: str = TupleField()
    comment: str = TupleField()
    base64Photo: str = TupleField()
    photoMimeType: str = TupleField()
