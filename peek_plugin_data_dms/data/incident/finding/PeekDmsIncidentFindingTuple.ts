import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";
import { PeekDmsIncidentFindingTypeEnum } from "./PeekDmsIncidentFindingTypeEnum";

@addTupleType
export class PeekDmsIncidentFindingTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFindingTuple";

    // This field allows customer specific data, that peek doesn't need to work
    data: { [key: string]: any } = {};

    incidentId: string;
    findingId: string;
    findingType: PeekDmsIncidentFindingTypeEnum =
        new PeekDmsIncidentFindingTypeEnum();

    comment: string;

    createdDate: Date;
    createdByName: string;
    createdByUserId: string;

    constructor(name: string = PeekDmsIncidentFindingTuple.tupleName) {
        super(name);
    }
}
