import logging
from datetime import datetime

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from .PeekDmsIncidentFindingTypeEnum import PeekDmsIncidentFindingTypeEnum
from vortex.Tuple import addTupleType, Tuple, TupleField

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsIncidentFindingTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFindingTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(comment="Second class data")

    incidentId: str = TupleField(comment="The incident id")
    findingId: str = TupleField(comment="The ID of this field finding")
    findingType: PeekDmsIncidentFindingTypeEnum = TupleField()

    comment: str = TupleField()

    createdDate: datetime = TupleField()
    createdByName: str = TupleField()
    createdByUserId: str = TupleField()
