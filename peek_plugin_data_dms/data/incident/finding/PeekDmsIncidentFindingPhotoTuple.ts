import { addTupleType } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";
import { PeekDmsIncidentFindingTuple } from "./PeekDmsIncidentFindingTuple";

@addTupleType
export class PeekDmsIncidentFindingPhotoTuple extends PeekDmsIncidentFindingTuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFindingTuple";

    photoNumber: number;
    relativePath: string;

    constructor() {
        super(PeekDmsIncidentFindingPhotoTuple.tupleName);
    }
}
