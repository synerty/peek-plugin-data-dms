import logging
from datetime import datetime

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from .PeekDmsIncidentFindingTypeEnum import PeekDmsIncidentFindingTypeEnum

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsIncidentFindingListItemTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFindingListItemTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(comment="Second class data")

    incidentId: str = TupleField(comment="The incident id")
    findingId: str = TupleField()
    findingType: PeekDmsIncidentFindingTypeEnum = TupleField()

    comment: str = TupleField()

    createdDate: datetime = TupleField()
    createdByName: str = TupleField()
    createdByUserId: str = TupleField()
