import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";
import { PeekDmsIncidentFindingTypeEnum } from "./PeekDmsIncidentFindingTypeEnum";

@addTupleType
export class PeekDmsIncidentFindingListItemTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFindingListItemTuple";

    // Second class data support.
    data: { [key: string]: any } = {};

    incidentId: string;
    findingId: string;
    findingType: PeekDmsIncidentFindingTypeEnum =
        new PeekDmsIncidentFindingTypeEnum();
    comment: string;

    createdDate: Date;
    createdByName: string;
    createdByUserId: string;

    constructor() {
        super(PeekDmsIncidentFindingListItemTuple.tupleName);
    }
}
