import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Incident, create new field finding
 *
 */
@addTupleType
export class PeekDmsIncidentFindingCreateCommentAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFindingCreateCommentAction";

    userId: string;
    incidentId: string;
    userDisplayName: string;
    comment: string;

    constructor() {
        super(PeekDmsIncidentFindingCreateCommentAction.tupleName);
    }
}
