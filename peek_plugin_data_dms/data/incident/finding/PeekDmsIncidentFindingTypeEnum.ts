import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentFindingTypeEnum extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFindingTypeEnum";

    // Not set
    static readonly NOT_SET = "NULL";

    // The real types
    static readonly COMMENT = "COMMENT";
    static readonly PHOTO = "PHOTO";
    static readonly LOG = "LOG";

    // UI States
    static readonly STATE_CHANGE_PENDING = "STATE_CHANGE_PENDING";

    constructor(public value: string = PeekDmsIncidentFindingTypeEnum.NOT_SET) {
        super(PeekDmsIncidentFindingTypeEnum.tupleName);
    }

    get niceName(): string {
        let lookup = {};
        lookup[PeekDmsIncidentFindingTypeEnum.NOT_SET] = "Null";
        lookup[PeekDmsIncidentFindingTypeEnum.COMMENT] = "Comment";
        lookup[PeekDmsIncidentFindingTypeEnum.PHOTO] = "Photo";
        lookup[PeekDmsIncidentFindingTypeEnum.LOG] = "Log";

        // UI States
        lookup[PeekDmsIncidentFindingTypeEnum.STATE_CHANGE_PENDING] =
            "Change Pending";
        return lookup[this.value];
    }

    get isComment(): boolean {
        return this.value === PeekDmsIncidentFindingTypeEnum.COMMENT;
    }

    get isPhoto(): boolean {
        return this.value === PeekDmsIncidentFindingTypeEnum.PHOTO;
    }
}
