import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Incident, Create new field finding
 *
 */
@addTupleType
export class PeekDmsIncidentFindingCreatePhotoAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFindingCreatePhotoAction";

    userId: string;
    userDisplayName: string;
    incidentId: string;
    comment: string;
    base64Photo: string;
    photoMimeType: string;

    constructor() {
        super(PeekDmsIncidentFindingCreatePhotoAction.tupleName);
    }
}
