from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix

from vortex.Tuple import addTupleType, TupleField
from vortex.TupleAction import TupleActionABC


@addTupleType
class PeekDmsIncidentFindingCreateCommentAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFindingCreateCommentAction"

    userId: str = TupleField()
    userDisplayName: str = TupleField()
    incidentId: str = TupleField()
    comment: str = TupleField()
