import logging

from vortex.Tuple import addTupleType, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.incident.finding.PeekDmsIncidentFindingTuple import (
    PeekDmsIncidentFindingTuple,
)

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsIncidentFindingPhotoTuple(PeekDmsIncidentFindingTuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFindingPhotoTuple"

    photoNumber: int = TupleField()
    relativePath: str = TupleField()
