from typing import Optional

from vortex.Tuple import Tuple, TupleField, addTupleType

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentFindingTypeEnum(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFindingTypeEnum"

    COMMENT = "COMMENT"
    PHOTO = "PHOTO"
    LOG = "LOG"

    value = TupleField(typingType=str)

    def __init__(self, value: Optional[str] = None):
        Tuple.__init__(self)
        self.value = value

    @property
    def isComment(self) -> bool:
        return self.value == self.COMMENT

    @property
    def isPhoto(self) -> bool:
        return self.value == self.PHOTO
