import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentFaultReportLookupItemTuple extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFaultReportLookupItemTuple";

    static readonly LOOKUP_WORK_TYPE = "LOOKUP_WORK_TYPE";
    static readonly LOOKUP_CAUSE = "LOOKUP_CAUSE";
    static readonly LOOKUP_CAUSE_GROUP = "LOOKUP_CAUSE_GROUP";
    static readonly LOOKUP_FAILED_ASSET = "LOOKUP_FAILED_ASSET";
    static readonly LOOKUP_FAILURE_MODE = "LOOKUP_FAILURE_MODE";

    parentKey: string | null;
    key: string;
    value: string;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsIncidentFaultReportLookupItemTuple.tupleName);
    }
}
