import logging
from datetime import datetime

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from .PeekDmsIncidentFieldStatusEnum import PeekDmsIncidentFieldStatusEnum
from vortex.Tuple import addTupleType, Tuple, TupleField

logger = logging.getLogger(__name__)


@addTupleType
class PeekDmsIncidentTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentTuple"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(comment="Second class data")

    incidentId: str = TupleField(comment="The incident id")
    incidentNumber: str = TupleField(comment="The incident name")
    incidentCategory: str = TupleField()
    incidentType: str = TupleField()
    priority: int = TupleField()
    status: str = TupleField()
    isSensitive: bool = TupleField()

    #: The name of the dispatcher sending the work
    dispatcherName: str = TupleField()

    #: The name of the field resource the work is assigned to
    fieldAssignee: str = TupleField()

    createdDate: datetime = TupleField()
    receivedDate: datetime = TupleField()
    estimatedRestorationDate: datetime = TupleField()
    restorationDate: datetime = TupleField()
    scheduledDate: datetime = TupleField()
    startDate: datetime = TupleField()
    completedDate: datetime = TupleField()
    lastUpdatedDate: datetime = TupleField()

    isPlanned: bool = TupleField()
    faultNumber: str = TupleField()
    operatingZone: str = TupleField()
    isConfirmed: bool = TupleField()
    numberOfCalls: int = TupleField()

    fieldStatus: PeekDmsIncidentFieldStatusEnum = TupleField(
        comment="The status of the incident in the field device."
    )

    incidentName: str = TupleField()
    message1: str = TupleField()
    message2: str = TupleField()
    comment: str = TupleField()

    primaryFeederName: str = TupleField()
    primaryName: str = TupleField()
    primaryAlias: str = TupleField()
    primaryId: str = TupleField()
    secondaryName: str = TupleField()
    secondaryAlias: str = TupleField()
    secondaryId: str = TupleField()
    componentDescription: str = TupleField()

    deadDeviceAlias: str = TupleField()
    deadDeviceId: str = TupleField()
    isDeviceDeadConfirmed: bool = TupleField()
