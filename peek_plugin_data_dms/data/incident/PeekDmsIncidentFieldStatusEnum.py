from typing import Optional

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.Tuple import Tuple, TupleField, addTupleType

"""
"RESOURCE_STATUS"             "DESCRIPTION"                 "RESOURCE_MESSAGE"            
"ACPT"                        "Accepted"                    "ResourceAccepted"            
"ASSN"                        "Assigned"                    "ResourceAssigned"            
"COMP"                        "Completed"                   "ResourceCompleted"           
"DISP"                        "Dispatched"                  "ResourceDispatched"          
"ONS"                         "On Site"                     "ResourceOnSite"              
"ENR"                         "En Route"                     "ResourceEnRoute"              
"CANC"                        "Cancelled"                   "ResourceCancelled"           
"LEFS"                        "Left Site"                   "ResourceLeftSite"            
"NEW"                         "Unassigned"                  
"SENT"                        "Sent"                        "ResourceSent"                
"RQST"                        "Requested"                   "ResourceRequested"           

"""


@addTupleType
class PeekDmsIncidentFieldStatusEnum(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFieldStatusEnum"

    ASSIGNED = "ASSIGNED"
    ACCEPTED = "ACCEPTED"
    COMPLETED = "COMPLETED"
    DISPATCHED = "DISPATCHED"
    EN_ROUTE = "EN_ROUTE"
    ON_SITE = "ON_SITE"
    CANCELLED = "CANCELLED"
    STOPPED = "STOPPED"
    UNASSIGNED = "UNASSIGNED"
    REQUESTED = "REQUESTED"

    value = TupleField(
        typingType=str, comment="The string value of the field status"
    )

    def __init__(self, value: Optional[str] = None):
        Tuple.__init__(self)
        self.value = value
