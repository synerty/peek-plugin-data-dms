import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsIncidentFaultReportTuple } from "./PeekDmsIncidentFaultReportTuple";

@addTupleType
export class PeekDmsIncidentUpdateFaultReportAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentUpdateFaultReportAction";

    userId: string;
    update: PeekDmsIncidentFaultReportTuple;

    constructor() {
        super(PeekDmsIncidentUpdateFaultReportAction.tupleName);
    }
}
