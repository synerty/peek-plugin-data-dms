from vortex.Tuple import addTupleType, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.incident.PeekDmsIncidentFaultReportTuple import (
    PeekDmsIncidentFaultReportTuple,
)


@addTupleType
class PeekDmsIncidentUpdateFaultReportAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentUpdateFaultReportAction"

    userId: str = TupleField()
    update: PeekDmsIncidentFaultReportTuple = TupleField()
