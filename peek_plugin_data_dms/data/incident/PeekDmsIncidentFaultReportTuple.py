from vortex.Tuple import addTupleType, TupleField, Tuple

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentFaultReportTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFaultReportTuple"

    incidentId: str = TupleField()
    reportId: str = TupleField()

    workType: str = TupleField()
    workTypeTitle: str = TupleField()

    failedAsset: str = TupleField()
    failedAssetTitle: str = TupleField()

    failureMode: str = TupleField()
    failureModeTitle: str = TupleField()

    failureComment: str = TupleField()

    causeGroup: str = TupleField()
    causeGroupTitle: str = TupleField()

    cause: str = TupleField()
    causeTitle: str = TupleField()

    causeComment: str = TupleField()

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(dict())
