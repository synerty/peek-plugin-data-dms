import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

/** Peek DMS Incident Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsIncidentContactDetailsTuple extends Tuple {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentContactDetailsTuple";

    incidentId: string;

    name: string;
    address: string;
    contactNumber: string;
    vechicalDetails: string;
    customerAgreementNumber: string;
    statementAtSiteNumber: string;
    emergencyServicesEventNumber: string;
    comment: string;

    // Second class data support.
    data: { [key: string]: any } = {};

    constructor() {
        super(PeekDmsIncidentContactDetailsTuple.tupleName);
    }
}
