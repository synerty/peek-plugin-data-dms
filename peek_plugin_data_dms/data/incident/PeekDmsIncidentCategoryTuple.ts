import { addTupleType, Tuple } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentCategoryTuple extends Tuple {
    public static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentCategoryTuple";

    // This field allows customer specific data, that peek doesn't need to work
    data: { [key: string]: any } = {};

    key: string;
    incidentCategory: string;
    incidentType: string;

    constructor() {
        super(PeekDmsIncidentCategoryTuple.tupleName);
    }
}
