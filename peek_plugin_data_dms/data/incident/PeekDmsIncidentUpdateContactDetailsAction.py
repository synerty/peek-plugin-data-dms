from vortex.Tuple import addTupleType, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.incident.PeekDmsIncidentContactDetailsTuple import (
    PeekDmsIncidentContactDetailsTuple,
)


@addTupleType
class PeekDmsIncidentUpdateContactDetailsAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentUpdateContactDetailsAction"

    userId: str = TupleField()
    update: PeekDmsIncidentContactDetailsTuple = TupleField()
