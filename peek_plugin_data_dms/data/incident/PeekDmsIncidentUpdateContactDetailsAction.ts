import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../plugin-pof-data-dms-names";
import { PeekDmsIncidentContactDetailsTuple } from "./PeekDmsIncidentContactDetailsTuple";

/** Peek DMS Incident Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsIncidentUpdateContactDetailsAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentUpdateContactDetailsAction";

    userId: string;
    update: PeekDmsIncidentContactDetailsTuple;

    constructor() {
        super(PeekDmsIncidentUpdateContactDetailsAction.tupleName);
    }
}
