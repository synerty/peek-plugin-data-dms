from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.TupleAction import TupleActionABC


@addTupleType
class PeekDmsIncidentFieldRejectAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFieldRejectAction"

    userId = TupleField(typingType=str)
    incidentId = TupleField(typingType=str)
    reason: str = TupleField()
