import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Incident Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsIncidentCtrlAssignAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentCtrlAssignAction";

    userId: string;
    incidentId: string;

    constructor() {
        super(PeekDmsIncidentCtrlAssignAction.tupleName);
    }
}
