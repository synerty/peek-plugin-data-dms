from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.TupleAction import TupleActionABC


@addTupleType
class PeekDmsIncidentFieldStopAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFieldStopAction"

    userId = TupleField(typingType=str)
    incidentId = TupleField(typingType=str)
    reason: str = TupleField()
