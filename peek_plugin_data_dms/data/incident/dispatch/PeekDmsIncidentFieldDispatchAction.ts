import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Incident Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsIncidentFieldDispatchAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFieldDispatchAction";

    userId: string;
    incidentId: string;
    reason: string;

    constructor() {
        super(PeekDmsIncidentFieldDispatchAction.tupleName);
    }
}
