import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Incident Dispatch - Field Acknoedment Action
 *
 */
@addTupleType
export class PeekDmsIncidentCtrlUpdateAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentCtrlUpdateAction";

    userId: string;
    incidentId: string;

    constructor() {
        super(PeekDmsIncidentCtrlUpdateAction.tupleName);
    }
}
