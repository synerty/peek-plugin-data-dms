from vortex.Tuple import addTupleType, Tuple, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentCtrlUpdateAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentCtrlUpdateAction"

    userId = TupleField(typingType=str)
    incidentId = TupleField(typingType=str)
