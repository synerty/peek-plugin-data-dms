from datetime import datetime

from vortex.Tuple import addTupleType, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentFieldUpdateArriveTimeAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFieldUpdateArriveTimeAction"

    userId: str = TupleField()
    incidentId: str = TupleField()
    reason: str = TupleField()
