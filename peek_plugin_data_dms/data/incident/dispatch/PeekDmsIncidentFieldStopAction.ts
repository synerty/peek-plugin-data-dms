import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentFieldStopAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFieldStopAction";

    userId: string;
    incidentId: string;
    reason: string;

    constructor() {
        super(PeekDmsIncidentFieldStopAction.tupleName);
    }
}
