import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentFieldRejectAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFieldRejectAction";

    userId: string;
    incidentId: string;
    reason: string;

    constructor() {
        super(PeekDmsIncidentFieldRejectAction.tupleName);
    }
}
