import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

/** Peek DMS Incident - Field Complete
 *
 */
@addTupleType
export class PeekDmsIncidentFieldCompleteAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFieldCompleteAction";

    userId: string;
    incidentId: string;
    reason: string;

    constructor() {
        super(PeekDmsIncidentFieldCompleteAction.tupleName);
    }
}
