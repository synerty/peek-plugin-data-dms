import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentCtrlTakebackAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentCtrlTakebackAction";

    userId: string;
    incidentId: string;

    constructor() {
        super(PeekDmsIncidentCtrlTakebackAction.tupleName);
    }
}
