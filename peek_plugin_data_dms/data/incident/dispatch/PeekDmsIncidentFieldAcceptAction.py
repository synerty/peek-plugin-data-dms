from typing import Optional

from vortex.Tuple import addTupleType, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from peek_plugin_data_dms.data.incident.dispatch.PeekDmsIncidentFieldUpdateArriveTimeAction import (
    PeekDmsIncidentFieldUpdateArriveTimeAction,
)


@addTupleType
class PeekDmsIncidentFieldAcceptAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFieldAcceptAction"

    userId: str = TupleField()
    incidentId: str = TupleField()
    reason: str = TupleField()

    updateArriveTimeAction: Optional[
        PeekDmsIncidentFieldUpdateArriveTimeAction
    ] = TupleField()
