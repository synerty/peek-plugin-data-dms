from vortex.Tuple import addTupleType, Tuple, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentCtrlAssignAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentCtrlAssignAction"

    userId = TupleField(typingType=str)
    incidentId = TupleField(typingType=str)
