from vortex.Tuple import addTupleType, TupleField
from vortex.TupleAction import TupleActionABC

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentFieldCompleteAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFieldCompleteAction"

    userId = TupleField(typingType=str)
    incidentId = TupleField(typingType=str)
    reason: str = TupleField()
