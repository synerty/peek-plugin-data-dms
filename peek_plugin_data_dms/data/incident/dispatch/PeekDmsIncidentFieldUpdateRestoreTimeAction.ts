import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";

@addTupleType
export class PeekDmsIncidentFieldUpdateRestoreTimeAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFieldUpdateRestoreTimeAction";

    userId: string;
    incidentId: string;
    reason: string;

    constructor() {
        super(PeekDmsIncidentFieldUpdateRestoreTimeAction.tupleName);
    }
}
