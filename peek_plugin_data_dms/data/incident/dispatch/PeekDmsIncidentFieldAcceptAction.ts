import { addTupleType, TupleActionABC } from "@synerty/vortexjs";
import { dataDmsTuplePrefix } from "../../plugin-pof-data-dms-names";
import { PeekDmsIncidentFieldUpdateArriveTimeAction } from "./PeekDmsIncidentFieldUpdateArriveTimeAction";

/** Peek DMS Incident Dispatch - Field Acknowledgment Action
 *
 */
@addTupleType
export class PeekDmsIncidentFieldAcceptAction extends TupleActionABC {
    static readonly tupleName =
        dataDmsTuplePrefix + "PeekDmsIncidentFieldAcceptAction";

    userId: string;
    incidentId: string;
    updateArriveTimeAction: PeekDmsIncidentFieldUpdateArriveTimeAction | null;
    reason: string;

    constructor() {
        super(PeekDmsIncidentFieldAcceptAction.tupleName);
    }
}
