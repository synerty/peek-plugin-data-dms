from datetime import datetime

from vortex.Tuple import addTupleType, Tuple, TupleField

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix
from vortex.TupleAction import TupleActionABC


@addTupleType
class PeekDmsIncidentFieldUpdateRestoreTimeAction(TupleActionABC):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFieldUpdateRestoreTimeAction"

    userId: str = TupleField()
    incidentId: str = TupleField()
    reason: str = TupleField()
