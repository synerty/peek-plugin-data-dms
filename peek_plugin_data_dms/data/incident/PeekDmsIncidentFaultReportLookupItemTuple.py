from typing import Optional

from vortex.Tuple import addTupleType, TupleField, Tuple

from peek_plugin_data_dms.PluginNames import dataDmsTuplePrefix


@addTupleType
class PeekDmsIncidentFaultReportLookupItemTuple(Tuple):
    __tupleType__ = dataDmsTuplePrefix + "PeekDmsIncidentFaultReportLookupItemTuple"

    parentKey: Optional[str] = TupleField()
    key: str = TupleField()
    value: str = TupleField()

    LOOKUP_WORK_TYPE = "LOOKUP_WORK_TYPE"
    LOOKUP_CAUSE = "LOOKUP_CAUSE"
    LOOKUP_CAUSE_GROUP = "LOOKUP_CAUSE_GROUP"
    LOOKUP_FAILED_ASSET = "LOOKUP_FAILED_ASSET"
    LOOKUP_FAILURE_MODE = "LOOKUP_FAILURE_MODE"

    # This field allows customer specific data, that peek doesn't need to work
    data: dict = TupleField(comment="Second class data")
