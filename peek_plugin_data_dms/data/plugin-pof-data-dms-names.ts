export let dataDmsFilt = { plugin: "peek_plugin_data_dms" };
export let dataDmsTuplePrefix = "peek_plugin_data_dms.";

export let dataDmsTupleOfflineServiceName = "peekPluginDataDms";
export let dataDmsObservableName = "peekPluginDataDms";

export let dataDmsActionProcessorName = "peekPluginDataDms";

export let dataDmsBaseUrl = "peek_plugin_data_dms";
