==============
Administration
==============

This plugin provides shared data structures or Vortex Tuples, that are used across
multiple plugins.

For example, The ENMAC SQL Plugin provides field switching tuples that are used by the
Field Switching plugin.

