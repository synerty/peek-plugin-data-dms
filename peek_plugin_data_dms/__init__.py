from typing import Type

from peek_plugin_base.agent.PluginAgentEntryHookABC import PluginAgentEntryHookABC
from peek_plugin_base.client.PluginClientEntryHookABC import PluginClientEntryHookABC
from peek_plugin_base.server.PluginLogicEntryHookABC import PluginLogicEntryHookABC
from peek_plugin_base.worker.PluginWorkerEntryHookABC import PluginWorkerEntryHookABC

from peek_plugin_data_dms._private.agent.PluginAgentEntryHook import (
    PluginAgentEntryHook,
)
from peek_plugin_data_dms._private.client.PluginClientEntryHook import (
    PluginClientEntryHook,
)
from peek_plugin_data_dms._private.server.PluginLogicEntryHook import (
    PluginLogicEntryHook,
)
from peek_plugin_data_dms._private.worker.PluginWorkerEntryHook import (
    PluginWorkerEntryHook,
)

__version__ = "0.0.0"


def peekLogicEntryHook() -> Type[PluginLogicEntryHookABC]:
    return PluginLogicEntryHook


def peekFieldEntryHook() -> Type[PluginClientEntryHookABC]:
    return PluginClientEntryHook


def peekOfficeEntryHook() -> Type[PluginClientEntryHookABC]:
    return PluginClientEntryHook


def peekWorkerEntryHook() -> Type[PluginWorkerEntryHookABC]:
    return PluginWorkerEntryHook


def peekAgentEntryHook() -> Type[PluginAgentEntryHookABC]:
    return PluginAgentEntryHook
