==========================
Peek - DMS Data Structures
==========================

This plugin provides common, peek standardised data structures that are used by
other plugins in the peek platform.


DMS Structures
--------------

Dispatch
````````

:PeekDmsJobDispatchAction: This action data structure represents a the control room
    dispatching a job to a field user.

:PeekDmsJobTakebackAction: This action data structure represents a the control room
    taking back a job from a field user.


:PeekDmsJobDispatchFieldAckAction: This action data structure represents a field users
    acknowledgement of a control centers dispatch command.

:PeekDmsJobTakebackFieldAckAction: This action represents the field users acknowledgment
    of the control centers request to take back a job.

